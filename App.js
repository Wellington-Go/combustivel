
import React,{useState} from 'react';
import {SafeAreaView,StyleSheet} from 'react-native';
import Gasolina from './componentes/gasolina.js'
import Etanol from './componentes/etanol.js'
import BtnCalcular from './componentes/btnCalcular.js'
import Resultado from './componentes/resultado.js'
import ImgResult from './componentes/imgResult.js'


export default function app() {

  const [gasolina,setGasolina] = useState(0)
  const [etanol,setEtanol] = useState(0)
  const [resultado,setResultado] = useState('')

  const Calcular=()=>{
    if(!gasolina){
      alert('Informe o preço')
      return
    }
    if(!etanol){
      alert('Informe o preço')
      return
    }

    let res
    let calc=((etanol/gasolina)* 100).toFixed(1)
    if(calc > 70){
      res = 'Gasolina'
    }else{
      res = 'Etanol'
    }
    alert('O Etanol esta custando' + calc + '% da Gasolina. Portato o melhor abastecer com '+ res)
    setResultado(res)
  }

  const limpa=()=>{
    setResultado('')
  }

  const setarGasolina=(v)=>{
    limpa()
    setGasolina(v)
  }

  const setarEtanol=(v)=>{
    limpa()
    setEtanol(v)
  }

  return (
   <SafeAreaView>
     <Gasolina aoModificar={setarGasolina}/>
     <Etanol aoModificar={setarEtanol}/>
     <BtnCalcular aoPress={Calcular}/>
     <Resultado resultado={resultado}/>
     <ImgResult comb={resultado.charAt(0)}/>
   </SafeAreaView>

  )

}

const Estilo = StyleSheet.create({

  principal:{
    padding:10
  }

});
