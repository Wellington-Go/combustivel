
import React from 'react';
import {Text,StyleSheet,View,TouchableHighlight} from 'react-native';



export default props=>{
  return (
    <View style={Estilo.bloco}>
        <Text> Calculo </Text>
        <TouchableHighlight
        style={Estilo.btn}
        onPress={props.aoPress()}
        >
            <Text style={Estilo.txtBtn}>Calcular</Text>
        </TouchableHighlight>
    </View>
  )

}

const Estilo = StyleSheet.create({
    bloco:{
        marginBottom:10
    },
    btn:{
        backgroundColor:'#800',
        justifyContent:'center',
        alignItems:'center',
        borderRadius:7,
        padding:15,
    },
    txtBtn:{
        textTransform:'uppercase',
        color:'#fff',
        fontSize:20,

    }

  

});