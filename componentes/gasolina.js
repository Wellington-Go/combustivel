
import React from 'react';
import {Text,StyleSheet,TextInput,View} from 'react-native';



export default props=>{
  return (
    <View style={Estilo.bloco}>
        <Text> Digite o preço da Gasolina </Text>
        <TextInput
        style={Estilo.txt}
        keyboardType='numeric'
        onChangeText={text=>props.aoModificar(text)}
        />
    </View>
  )

}

const Estilo = StyleSheet.create({
    bloco:{
        marginBottom:10
    },
    txt:{
        borderColor:'#000',
        borderWidth:2,
        borderRadius:7,
    }

  

});