
import React from 'react';
import {StyleSheet,View,Image} from 'react-native';



export default props=>{
  return (
    <View style={Estilo.bloco}>
        {
            props.comb == ''?
            <Image source={require('../assets/posto.png')}
                style={Estilo.comb}/>
            :
            props.comb == 'G' ?
            <Image source={require('../assets/gasolina.png')}
                style={Estilo.comb}/>
            :
            <Image source={require('../assets/etanol.png')}
                style={Estilo.comb}/>

        }
    </View>
  )

}

const Estilo = StyleSheet.create({
    bloco:{
        marginBottom:10,
        justifyContent:'center',
        alignItems:'center'
    },
    comb:{
        resizeMode:'stretch',
    }

  

});